from dearpygui import dearpygui as dpg
from trainerbase.gui import (
    CodeInjectionUI,
    GameObjectUI,
    SeparatorUI,
    SpeedHackUI,
    TeleportUI,
    add_components,
    simple_trainerbase_menu,
)

from injections import infinite_ammo, infinite_health, infinite_mana
from objects import last_used_item_count
from teleport import tp


@simple_trainerbase_menu("Dishonored", 740, 280)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                CodeInjectionUI(infinite_health, "Infinite Health", "F1"),
                CodeInjectionUI(infinite_mana, "Infinite Mana", "F2"),
                CodeInjectionUI(infinite_ammo, "Infinite Ammo", "F3"),
                SeparatorUI(),
                GameObjectUI(last_used_item_count, "Last Used Item Count"),
                SeparatorUI(),
                SpeedHackUI(),
            )

        with dpg.tab(label="Teleport"):
            add_components(TeleportUI(tp))
