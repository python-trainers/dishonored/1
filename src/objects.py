from trainerbase.gameobject import GameFloat, GameUnsignedInt
from trainerbase.memory import Address, pm

from memory import last_used_item_count_pointer


player_base_address = Address(pm.base_address + 0x105F628)

player_coords_struct_address = player_base_address + [0xC4]
player_x = GameFloat(player_coords_struct_address)
player_y = GameFloat(player_coords_struct_address + 0x4)
player_z = GameFloat(player_coords_struct_address + 0x8)

last_used_item_count = GameUnsignedInt(Address(last_used_item_count_pointer, [0x4]))
