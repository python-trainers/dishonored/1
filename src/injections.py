from trainerbase.codeinjection import AllocatingCodeInjection, CodeInjection, MultipleCodeInjection
from trainerbase.memory import pm

from memory import last_used_item_count_pointer


infinite_health = AllocatingCodeInjection(
    pm.base_address + 0x6B2C2E,
    """
        mov dword [esi + 0x344], 10000
        mov ecx, [esi + 0x344]
    """,
    original_code_length=6,
)

infinite_mana = AllocatingCodeInjection(
    pm.base_address + 0x6A93F8,
    """
        mov dword [esi + 0xA60], 200
        mov ecx, [esi + 0xA60]
    """,
    original_code_length=6,
)

infinite_ammo = CodeInjection(pm.base_address + 0x8052CB, "nop\n" * 2)

update_last_used_item_count_pointer = MultipleCodeInjection(
    AllocatingCodeInjection(  # spend an item
        pm.base_address + 0x80F8F9,
        f"""
            dec ebx
            and edx, ebx
            mov [esi + 0x4], edx

            mov [{last_used_item_count_pointer}], esi
        """,
        original_code_length=6,
    ),
    AllocatingCodeInjection(  # pick up an item
        pm.base_address + 0x80B7D1,
        f"""
            dec eax
            and eax, edi
            add [ecx + 0x4], eax

            mov [{last_used_item_count_pointer}], ecx
        """,
        original_code_length=6,
    ),
)
